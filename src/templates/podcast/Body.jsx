import React from 'react'
import { 
  Container, Grid, Typography, Button, Box, Card, CardMedia, Fab, Hidden 
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import EditIcon from '@material-ui/icons/Edit'
import CastIcon from '@material-ui/icons/Cast'
import RssFeedIcon from '@material-ui/icons/RssFeed'
import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import laptop1 from '../../img/laptop-1.jfif'
import laptop2 from '../../img/laptop-2.jpg'
import laptop3 from '../../img/laptop-3.jpg'

const useStyles = makeStyles(theme => ({
  intro: {
    backgroundImage: 'linear-gradient(135deg, #651fff 40%, #7c4dff 60%)',
    height: 400,
    color: 'white'
  },
  introHeadline: {
    fontWeight: 'bold',
    fontSize: '32px',
    [theme.breakpoints.up('md')]: {
      fontSize: '40px'
    },
    marginBottom: theme.spacing(2)
  },
  introSubtitle: {
    marginBottom: theme.spacing(3)
  },
  ctaBtn: {
    background: 'white',
    color: theme.palette.primary.main,
    textTransform: 'capitalize'
  },
  container: {
    height: '100%',
    paddingLeft: theme.spacing(3)
  },
  accent: {
    color: theme.palette.accent.main
  },
  step: {
    border: `5px solid ${theme.palette.primary.main}`,
    borderRadius: '50%',
    padding: theme.spacing(2),
    height: 75,
    width: 75,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  card: { 
    position: 'relative',
    height: 200
  },
  overlay: {
    position: 'absolute',
    background: 'rgba(0,0,0, 0.45)', 
    left: 0, top: 0, 
    bottom: 0, right: 0, 
    height: '100%',
    color: 'white',
    display: 'flex',
    alignItems: 'flex-end',
    padding: '30px',
    
  },
  outro: {
    backgroundImage: 'linear-gradient(135deg, #651fff, #7c4dff, #651fff)',
    height: 200,
    color: 'white',
    textAlign: 'center'
  },
}))

const steps = [
  {text: 'Type your content', icon: <EditIcon />, image: laptop1},
  {text: 'Text to speech', icon: <CastIcon />, image: laptop2},
  {text: 'Your podcast is live!', icon: <RssFeedIcon />, image: laptop3}
]
const features = [
  'Type your podcast as if it were a blog',
  'Include audio snippets, songs, tunes, jingles...',
  'Voices in multiples lanuages and accents',
  'Single click text-to-speech'
]

export default function Body() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className={classes.intro}>
        <Container className={classes.container}>
          <Grid 
            container  
            alignItems='center' 
            className={classes.container}
          >
            <Grid item xs={11} sm={8} md={7} lg={6}>
              <Typography className={classes.introHeadline} paragraph>
                {'Create a podcast, without recording a thing'} 
              </Typography>
              <Typography className={classes.introSubtitle} paragraph>
                {`Produce your next podcast simply by typing the content and
                 Podcastly will convert it to speech. No more microphones, 
                 audio softwares or even re-recording your mistakes.`}
              </Typography>
              <Button 
                size='large' 
                color="tertiary"
                variant="contained"
                className={classes.ctaBtn}
              >
                Discover More
              </Button>
            </Grid>
          </Grid>
        </Container>
      </div>
      {/* How to use */}
      <Container maxWidth="md" style={{padding: '30px 0px'}}>
        <Box mb={3}>
          <Typography variant="h5" align="center">
            <b>{`It's easy to create a podcast with Podcastly`}</b>
          </Typography>
        </Box>
        <Grid container justify="center">
          {steps.map((step, i) => (
            <Grid item xs={10} sm={6} md={4} style={{padding: 10}}>
              <Card className={classes.card} elevation={0}>
                <CardMedia component="img" src={step.image} />
                <div className={classes.overlay}>
                  <Grid>  
                    <Box style={{display: 'block'}}>
                      {step.icon}
                    </Box>
                    <Typography variant="h6" color="white">
                      {(i + 1) + '.'} {step.text}
                    </Typography>
                  </Grid>
                </div>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
      {/* Features section*/}
      <Container>
        <Grid container justify="flex-end">
          <Grid item xs={12} md={7} style={{padding: '0px 10px 0px'}}>
            <Box my={1}>
              <Fab color="primary" size="small">
                <RssFeedIcon />
              </Fab>
            </Box>
            <Typography variant="h5">
               <b>{'An exceptionaly intuitive editor to create podcasts'}</b> 
            </Typography>
          </Grid>
        </Grid>
        <Grid container justify="space-between">
          <Grid item xs={11} sm={8} md={4} style={{padding: 10}}>
            <Box style={{position: 'relative'}} mb={10}>
              <Card  elevation={0} style={{height: 200, width: '85%'}}>
                <CardMedia component="img" src={laptop2} style={{height: '100%'}} />
              </Card>
              <Card elevation={0} style={{position: 'absolute', height: 220, top: 50, left: 50}}>
                <CardMedia component="img" src={laptop3} style={{height: '100%'}}/>
              </Card>
            </Box>
          </Grid>
          <Grid item xs={12} md={7} style={{padding: 10}}>
            <Grid container justify="space-between">
              {features.map(feature => (
                <Grid item xs={6} sm={5}>
                  <Typography variant="body1">
                    <b>{feature}</b> 
                  </Typography>
                  <Typography paragraph variant="body2">
                    {`Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Cras justo odio, dapibus ac dolor sit ametdolor sit amet.`}
                  </Typography>
                </Grid>
              ))}
            </Grid>
            <Box>
              <Button color="primary">
                <ArrowRightIcon />
                Register for a Free Trial
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Container>
      <Container>
        <Grid container>
          <Grid item xs={12} md={6} style={{padding: '0px 10px 0px'}}>
            <Box my={1}>
              <Fab color="primary" size="small">
                <RssFeedIcon />
              </Fab>
            </Box>
            <Typography variant="h5">
               <b>{`Podcast hosting & analytics`}</b> 
            </Typography>
          </Grid>
        </Grid>
        <Grid container style={{padding: '15px 0px'}} justify="space-between">
          <Grid item xs={12} md={7} style={{padding: 10}}>
            <Grid container justify="space-between">
              {features.map(feature => (
                <Grid item xs={6} sm={5} md={5}>
                  <Typography variant="body1">
                    <b>{feature}</b> 
                  </Typography>
                  <Typography paragraph variant="body2">
                    {`Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Cras justo odio, dapibus ac dolor sit ametdolor sit amet.`}
                  </Typography>
                </Grid>
              ))}
            </Grid>
            <Box>
              <Button color="primary">
                <ArrowRightIcon />
                Sign up now
              </Button>
            </Box>
          </Grid>
          <Hidden smDown>
            <Grid item md={4} container justify="flex-end">
              <Box style={{position: 'relative'}} mb={10}>
                <Card  elevation={0} style={{height: 200, width: '85%'}}>
                  <CardMedia component="img" src={laptop2} style={{height: '100%'}} />
                </Card>
                <Card elevation={0} style={{position: 'absolute', height: 250, top: 50, left: 50}}>
                  <CardMedia component="img" src={laptop3} style={{height: '100%'}}/>
                </Card>
              </Box>
            </Grid>
          </Hidden>
        </Grid>
      </Container>
      <div className={classes.outro}>
        <Grid 
          container 
          justify="center" 
          alignItems="center" 
          style={{height: '100%'}}
        >
          <Grid item xs={10}>
            <Typography variant="h4" align="center">
              <b>{`Ready to create a podcast?`}</b>
            </Typography>
            <Box my={2}>
              <Button 
                size='large' 
                color="tertiary"
                variant="contained"
                className={classes.ctaBtn}
              >
                Get Started for free
              </Button>
            </Box>
          </Grid>
        </Grid>
      </div>
    </React.Fragment>
  )
}