import React from 'react'
import { Typography, Grid, Link, IconButton } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import TwitterIcon from '@material-ui/icons/Twitter'
import FacebookIcon from '@material-ui/icons/Facebook'
import InstagramIcon from '@material-ui/icons/Instagram'

const useStyles = makeStyles(theme => ({
  container: {
    padding: '20px 15px',
    borderTop: `1px solid ${theme.palette.divider}`,
    textAlign: 'center'
  },
  share: {
    fontSize: '32px',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '50%',
    margin: theme.spacing(1)
  },
  link: {
    display: 'inline-block',
    margin: theme.spacing(1)
  }
}))

export default function Footer({title}) {
  const classes = useStyles();

  const share = [<TwitterIcon />, <FacebookIcon />, <InstagramIcon />]
  const links = ['Pricing', 'Contact', 'Privacy Policy', 'Terms of Service']

  return (
    <React.Fragment>
      <Grid 
        container
        className={classes.container}
        justify="center"
      >
        <Grid item xs={12} sm={10} md={8}>
          {links.map(link => (
            <Typography 
              color="textSecondary" 
              variant="body2" 
              className={classes.link}
            >
              <Link color="inherit">{link}</Link>
            </Typography>
          ))}
        </Grid>
        <Grid item xs={12} sm={10} md={8}>
          {share.map(el => (
            <IconButton size="medium" className={classes.share}>
              {el}
            </IconButton>
          ))}
        </Grid>
        <Grid item xs={12} sm={10} md={8}>
          <Typography color="textSecondary" variant="body2">
            <span>&copy; 2008 - { new Date().getFullYear() } </span>
            <Link color="inherit">{title}</Link>
            {` All rights reserved. Lorem ipsum dolor sit amet, consectetur 
            adipisicing elit. Est blanditiis dolorem culpa incidunt minus,
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est 
            blanditiis dolorem culpa incidunt minus.`}
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}