import React, { useState } from 'react';
import { 
  AppBar, Box, Drawer, Divider, Toolbar, Button, List, ListItem, ListItemText, Typography,
  ButtonGroup, Hidden, IconButton, useScrollTrigger, Slide
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { makeStyles } from '@material-ui/core/styles'; 

const useStyles = makeStyles(theme => ({
  appBar: theme.mixins.container,
  drawerPaper: {
    width: 240
  },
  logo: {
    marginRight: theme.spacing(3)
  },
  logoIcon: {
    marginRight: theme.spacing(1),
    color: theme.palette.primary.main
  },
  navBtn: {
    textTransform: 'capitalize',
    fontWeight: 'bold',
    margin: '0 10px'
  },
  OpenDrawerBtn: {
    marginLeft: 'auto',
    color: theme.palette.primary.main,
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  closeDrawerBtn: {
    display: 'flex',
    alignItems: 'center',
    ...theme.mixins.toolbar
  }
}))
 
const HideOnScroll = ({children, window}) => {
  const trigger = useScrollTrigger({
    target: window ? window() : undefined
  })
  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  )
}
/*
const ElevateOnScroll = ({children, window}) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined
  })

  return (
    React.cloneElement(children, {
      elevation: trigger ? 4 : 0
    })
  )
}*/


export default function Header({title, links, icon}, props) {
  const classes = useStyles();
  const [navDrawer, setNavDrawer] = useState(false);

  const toggleDrawer = () => {
    setNavDrawer(!navDrawer);
  }

  return (
    <React.Fragment>
      <HideOnScroll {...props}>
        <AppBar color="white" className={classes.appBar} elevation={0}>
          <Toolbar>
            <Box className={classes.logoIcon}>
              {icon}
            </Box>
            <Typography 
              component="h1" 
              variant="h5" 
              color="inherit"
              className={classes.logo}
            >
              {title}
            </Typography>
            <Hidden smDown>
              <ButtonGroup>
                {links.map(link => (
                  <Button className={classes.navBtn} variant="text">
                    {link}
                  </Button>
                ))}
              </ButtonGroup>
                
              <Button 
                className={classes.navBtn} 
                variant="text" 
                style={{marginLeft: 'auto'}}
              >
                Login
              </Button>

              <Button  
                className={classes.navBtn} 
                color="primary"
                variant="contained"
                disableElevation
              >
                Sign Up
              </Button>
            </Hidden>
            
            <IconButton  
              className={classes.OpenDrawerBtn}
              onClick={toggleDrawer}
            >
              <MenuIcon/>
            </IconButton>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Box mt={7} />
      <Drawer 
        variant="persistent" 
        onClose={toggleDrawer} 
        open={navDrawer}
        anchor="right"
        classes={{paper: classes.drawerPaper}}
      >
        <Box className={classes.closeDrawerBtn}>
          <IconButton 
            onClick={toggleDrawer}
            color="primary"
          >
            <ChevronRightIcon />
          </IconButton>
        </Box>
        <Divider />
        <List>
          {links.map(link => (
            <ListItem button>
              <ListItemText primary={link} />
            </ListItem>
          ))}
        </List>
      </Drawer>
    </React.Fragment>
  )
}
