import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import { 
  Typography, Link, Container, Avatar, TextField, FormControlLabel, 
  Checkbox, Button, Grid, Box
} from '@material-ui/core';


  const Copyright = () => (
    <Typography variant="body2" color="textSecondary" align="center">
      <span>&copy; </span> {new Date().getFullYear()} {' '}
      <Link color="inherit" href="/">SignInExample.com</Link>
    </Typography>
  )

  const useStyle = makeStyles(theme => ({
    paper: {
      marginTop: '20px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    },
    avatar: {
      margin: theme.spacing(1),
      background: theme.palette.secondary.main
    },
    form: {
      width: '100%',
      marginTop: theme.spacing(1)
    },
    submit: {
      margin: theme.spacing(2, 0, 1)
    }
  }));

export default function SignIn() {
  const classes = useStyle();
  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar} />
        <Typography component="h1" variant="h5">
          Sign In
        </Typography>
        <form className={classes.form} noValidate>
          <TextField 
            label="Email Address"
            variant="outlined"
            required
            autoComplete="email"
            autoFocus
            fullWidth
            margin="normal"
            id="email"
          />
          <TextField 
            label="Password"
            variant="outlined"
            required
            autofocus
            fullWidth
            margin="normal"
            id="password"
            type="password"
            autoComplete="current-password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button 
            className={classes.submit}
            variant="contained"
            color="primary"
            fullWidth
            type="submit"
            size="large"
          >
            Sign In
          </Button>
          <Grid container style={{marginTop: '16px'}}>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account yet? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  )
};
