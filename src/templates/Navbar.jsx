import React, { useState } from 'react';
import {
  AppBar, Toolbar, IconButton, Typography, Badge, Drawer, Divider, List,
  ListItem, ListItemText, Hidden, Button  
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { makeStyles } from '@material-ui/core/styles';


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  closeMenuButton: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  toolbar: {
    paddingRight: 34 
  },
  title: {
    flexGrow: 1
  },
  navLinks: {
    marginRight: theme.spacing(1),
    fontSize: 13,
    padding: '0 10px',
  }
}));

export default function Navbar() {
  const classes = useStyles();
  const links = ['About', 'Services', 'Blog','Contact']

  // Drawer state and handlers
  const [mobileOpen, setMobileOpen] = useState(false);
  
  const toggleDrawer = () => {
    setMobileOpen(!mobileOpen);
  }

  return (
    <>
      <AppBar elevation={0}>
        <Toolbar className={classes.toolbar}>
          <IconButton 
            edge="start" 
            color="inherit"
            aria-label="Open drawer" 
            className={classes.menuButton}
            onClick={toggleDrawer}
          >
            <MenuIcon />
          </IconButton>
          <Typography 
            component="h1" 
            variant="h6" 
            noWrap 
            className={classes.title}
            color="inherit"
          > 
            Material UI 
          </Typography>
          <Hidden smDown implementation="css">
            {links.map(link => (
              <Button 
                variant="text" 
                size="small" 
                href="/" 
                className={classes.navLinks} 
                color="inherit"
              >
                {link}
              </Button>
            ))}
          </Hidden>
          <IconButton color="inherit" style={{marginLeft: 20}}>
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Hidden >
        <Drawer 
          variant="persistent" 
          open={mobileOpen}
          onClose={toggleDrawer}
          classes={{paper: classes.drawer}}
        >
          <div className={classes.closeMenuButton}>
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          {links.map(link => (
            <List>
              <ListItem button>
                <ListItemText primary={link} />
              </ListItem>
            </List>
          ))}
        </Drawer>
      </Hidden>
    </>
  )
}

