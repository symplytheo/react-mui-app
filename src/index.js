import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import Podcast from './Podcast';

ReactDOM.render(
  <React.StrictMode>
    <Podcast />
  </React.StrictMode>,
  document.getElementById('root')
);

