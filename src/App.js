import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import {green} from '@material-ui/core/colors';
//import SignIn from './templates/SignIn';
import Navbar from './templates/Navbar';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#651fff'
    },
    secondary: {
      main: green['A400']
    }
  }
})

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    backgroundColor: theme.palette.primary.main
  }
}))

export default function App() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <CssBaseline />
        <Navbar />
        Another day
      </div>
    </ThemeProvider>
  );
}
