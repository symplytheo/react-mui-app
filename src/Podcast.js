import React from 'react';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import {deepPurple} from '@material-ui/core/colors'
import Header from './templates/podcast/Header'
import Body from './templates/podcast/Body'
import Footer from './templates/podcast/Footer'
import CastIcon from '@material-ui/icons/Cast';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: deepPurple['A400'],
    },
    secondary: {
      main: deepPurple[200]
    },
    accent: {
      main: deepPurple[500],
    },
    tertiary: {
      main: '#FFFFFF',
    }
  },
  typography: {
    fontFamily: ['Poppins', 'Arial']
  }
})

export default function Podcast() {
  const navLinks = ['Features', 'Examples', 'Pricing', 'Support'];

  return ( 
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header icon={<CastIcon/>} title="podcastly" links={navLinks} />
      <main>
        <Body />
      </main>
      <Footer title="Podcastly." />
    </ThemeProvider>
  )
}