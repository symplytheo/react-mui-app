import React, { Component } from 'react';

class FlavorForm extends Component {
  constructor(props){
    super(props);
    this.state = {flavor: ['coconut', 'vanilla']};
  }

  handleChange(e) {
    this.setState({flavor: e.target.value});
  }

  handleSubmit(e) {
    alert('Your favorite flavor is: ' + this.state.flavor);
    e.preventDefault();
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <select
          value={this.state.flavor} 
          onChange={this.handleChange.bind(this)}
          multiple={true}
        >
          <option value="grape">Grape</option>
          <option value="coconut">Coconut</option>
          <option value="vanilla">Vanilla</option>
        </select>
        <input type="submit" value="Choose Favorite"/>
      </form>
    );
  }
}

export default class Form extends Component {
  constructor(props){
    super(props);
    this.state = {value: ''};
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  handleSubmit(e) {
    alert('An essay was submitted: ' + this.state.value);
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <label>
            Name:
            <textarea 
              value={this.state.value} 
              onChange={this.handleChange.bind(this)}
              placeholder={'Write an Essay'}
            />
          </label>
          <input type="submit" value="Submit" className="btn"/>
        </form>
        <p>
          <FlavorForm/>
        </p>
      </div>
    );
  }
}