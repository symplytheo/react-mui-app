import React, {Component} from 'react';

function ListNumber(props) {
  return <li>{props.value.text}</li>
}

function NumList(props) {
  const numbers = props.numbers;
  return (
  <ul>
    {numbers.map(num =>
      <ListNumber key={num.id} value={num} />)
    }
  </ul>
  )
}

export default class List extends Component {
  render() {

    const myList = [
      {text: 'Yamarita', id: 1},
      {text: 'Beans', id: 2},
      {text: 'Egg', id: 3}
    ]

    return (
      <NumList numbers={myList}/>
    );
  }
}