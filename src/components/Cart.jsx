import React from 'react';

function AddButton(props) {
  return (
    <button onClick={props.onClick} className="btn">
      +
    </button>
  );
}

function RemoveButton(props) {
  return (
    <button onClick={props.onClick} className="btn">
      -
    </button>
  )
}

export default class CartControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {cart: 0}
  }

  increaseCart() {
    this.setState(state => ({cart: state.cart + 1}));
  }
  
  reduceCart() {
    this.setState({cart: this.state.cart - 1});
  }

  render() {
    const cartNumber = this.state.cart;

    return (
      <div className="Cart">
        <b>Cart: {cartNumber}</b>
        <RemoveButton onClick={this.reduceCart.bind(this)} />
        <AddButton onClick={this.increaseCart.bind(this)} />
      </div>
    )
  }
};
