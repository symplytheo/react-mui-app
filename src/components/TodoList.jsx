import React, {Component} from 'react';
import Button from '@material-ui/core/Button';

class Listing extends Component {
  render() {
    return (
      <li style={{textDecoration: this.props.done ? 'line-through' :'none'}}>
        <input type="checkbox" checked={this.props.done} onClick={this.props.onClick}/>
        {this.props.text}
      </li>
    )
  }
}

class TodoItem extends Component {
  render() {
    return (
      <ul style={{listStyle: 'none'}}>
        {this.props.list.map((item, i) =>
          <Listing 
            key={item.toString()} 
            done={item.done} 
            text={item.text}
            onClick={() => {this.props.onClick(i)}} 
          />
        )}
      </ul>
    )
  }
}

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {text: 'Buy groceries', done: false},
        {text: 'Learn React', done: true},
        {text: 'Improve on Vue', done: true}
      ],
      completed: 'man',
      newItem: '',
      title: 'My Shopping List'
    }
  };

  changeTitle = (e) => {
    this.setState({ title: e.target.value });
  }

  /*addItem = (e) => {
    if (e.key === 'Enter') {
      this.setState({
        items: [ {text: e.target.value.trim(), done: false}, ...this.state.items]
      });
      e.target.value = ''
    }
  } */

  addNewItem = () => {
    this.setState({
      items: [{text: this.state.newItem, done:false}, ...this.state.items]
    });
  }

  checkItem = (index) => {
    const newArr = [...this.state.items];
    //newArr.splice(index, 1); - to delete item
    newArr[index].done = !newArr[index].done;
    this.setState({items: newArr})
  }

  componentDidMount() {
    const newArr = [...this.state.items];
    const doneArr = newArr.map(el => el.done);
    this.setState({completed: doneArr});
  }

  render() {
    return (
      <div>
        <h1>{this.state.title}</h1>
        <p>
          <TodoItem list={this.state.items} 
          onClick={this.checkItem}/>
        </p>
        <p>
          <span style={{marginRight: '15px'}}>Change title:</span>
          <input
            placeholder='add new item' 
            onInput={(e => this.setState({newItem: e.target.value}))}
            value={this.state.newItem}
          />
          <Button 
            onClick={this.addNewItem} 
            variant="contained" 
            color="primary"
            >
              Add
            </Button>
        </p>
        <p> 
          <span style={{marginRight: '15px'}}>Change title:</span>
          <input
            value={this.state.title} 
            onChange={this.changeTitle} 
          />
        </p>
      </div>
    );
  }
}

export default TodoList;