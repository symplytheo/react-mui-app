import React, { Component } from 'react';

function UserGreeting() {
  return <h1>Welcome Back Dude</h1>;
}

function GuestGreeting() {
  return <h1>Please Login</h1>;
}

function Greeting (props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <UserGreeting />;
  }
  return <GuestGreeting />;
}

function LoginButton(props) {
  return (
    <button onClick={props.onClick}>
      Login
    </button>
  );
}

function LogoutButton(props) {
  return (
    <button onClick={props.onClick}>
      Logout
    </button>
  )
}

class LoginControl extends Component {
  constructor(props) {
    super(props);
    this.state = {isLoggedIn: false}
  }

  handleLoginClick() {
    this.setState({isLoggedIn: true});
  }
  
  handleLogoutClick() {
    this.setState({isLoggedIn: false});
  }

  render() {
    const isLoggedIn = this.state.isLoggedIn
    return (
      <div>
        <Greeting isLoggedIn={isLoggedIn} />
        {isLoggedIn 
          ? <LogoutButton 
              onClick={this.handleLogoutClick.bind(this)} 
            />
          : <LoginButton 
              onClick={this.handleLoginClick.bind(this)} 
            /> 
        }
      </div>
    )
  }
}



export default LoginControl;