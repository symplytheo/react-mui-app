import React from 'react';

class Clock extends React.Component {
  constructor(props){
    super(props);
    this.tick =this.tick.bind(this)
    this.state = {date: new Date()}
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick() {
    this.setState({
      date: new Date()
    })
  }

  render() {
    return (
      <div className="Clock">
        <h1>Hello Man</h1>
        <p>
          The time is <b>{this.state.date.toLocaleTimeString()}</b>
        </p>
      </div>
    )
  }  
}

export default Clock;